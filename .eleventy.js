const eleventyNavigationPlugin = require('@11ty/eleventy-navigation')

// Import filters
const w3DateFilter = require('./src/_filters/w3-date-filter.js')

module.exports = function (eleventyConfig) {
  // Filters
  eleventyConfig.addFilter('w3DateFilter', w3DateFilter)

  // Plugins
  eleventyConfig.addPlugin(eleventyNavigationPlugin)

  // Pass through
  eleventyConfig.addPassthroughCopy('README.md')
  eleventyConfig.addPassthroughCopy('src/assets/imgs')
  eleventyConfig.addPassthroughCopy('src/assets/css')
  eleventyConfig.addPassthroughCopy('src/assets/js')
  eleventyConfig.addPassthroughCopy('src/assets/files')
  eleventyConfig.addPassthroughCopy('src/robots.txt')
  eleventyConfig.addPassthroughCopy('src/site.webmanifest')
  eleventyConfig.addPassthroughCopy('src/browserconfig.xml')

  // Watch
  eleventyConfig.addWatchTarget('./src/assets/scss/')

  // Universal Shortcodes
  eleventyConfig.cloudinaryCloudName = 'kuksoolwonwoodbridge'
  eleventyConfig.addShortcode('cloudinaryImage', function (path, transforms, alt) {
    return `<img src="https://res.cloudinary.com/${eleventyConfig.cloudinaryCloudName}/${transforms}/${path}" alt="${alt}">`
  })

  return {
    dir: {
      input: 'src',
      output: 'dist'
    },
    passthroughFileCopy: true,
    htmlTemplateEngine: 'njk',
    markdownTemplateEngine: 'njk',
    templateFormats: ['njk', 'md', 'html']
  }
}
